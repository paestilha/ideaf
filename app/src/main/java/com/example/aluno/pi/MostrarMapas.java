package com.example.aluno.pi;

import android.content.Intent;
import android.location.LocationListener;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MostrarMapas extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private Marker currentLocationMaker;
    private LatLng currentLocationLatLong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_mapas);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                if (marker != null && marker.getTitle().equals("Museu de Ecologia Fritz Muller")) {
                    Intent intent1 = new Intent(MostrarMapas.this, MuseuFritzMuller.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Biblioteca Municipal Dr. Fritz Muller")) {
                    Intent intent1 = new Intent(MostrarMapas.this, Biblioteca.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Museu de Hábitos e costumes")) {
                    Intent intent1 = new Intent(MostrarMapas.this, MuseuHabitos.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Museu da Cerveja")) {
                    Intent intent1 = new Intent(MostrarMapas.this, MuseuCerveja.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Museu da família colonial")) {
                    Intent intent1 = new Intent(MostrarMapas.this, MuseuFamilia.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Museu da arte")) {
                    Intent intent1 = new Intent(MostrarMapas.this, MuseuArte.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Mausoleu Dr. Blumenau")) {
                    Intent intent1 = new Intent(MostrarMapas.this, Mausoleu.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Teatro Carlos Gomes")) {
                    Intent intent1 = new Intent(MostrarMapas.this, Teatro.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Museu Hering")) {
                    Intent intent1 = new Intent(MostrarMapas.this, MuseuHering.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Vila Germânica")) {
                    Intent intent1 = new Intent(MostrarMapas.this, Vila.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Catedral São Paulo Apóstolo")) {
                    Intent intent1 = new Intent(MostrarMapas.this, Catedral.class);
                    startActivity(intent1);
                }
                if (marker != null && marker.getTitle().equals("Parque Ramiro Ruediger")) {
                    Intent intent1 = new Intent(MostrarMapas.this, Parque.class);
                    startActivity(intent1);
                }
            }
        });

        LatLng Mf = new LatLng(-26.910145, -49.044080);
        mMap.addMarker(new MarkerOptions().position(Mf).title("Museu de Ecologia Fritz Muller"));

        LatLng Mhc = new LatLng(-26.921320, -49.057819);
        mMap.addMarker(new MarkerOptions().position(Mhc).title("Museu de Hábitos e costumes"));

        LatLng Mc = new LatLng(-26.921679, -49.058787);
        mMap.addMarker(new MarkerOptions().position(Mc).title("Museu da Cerveja"));

        LatLng Mfa = new LatLng(-26.922367, -49.057650);
        mMap.addMarker(new MarkerOptions().position(Mfa).title("Museu da família colonial"));

        LatLng Bfm = new LatLng(-26.922236, -49.057939);
        mMap.addMarker(new MarkerOptions().position(Bfm).title("Biblioteca Municipal Dr. Fritz Muller"));

        LatLng Mar = new LatLng(-26.922368, -49.058856);
        mMap.addMarker(new MarkerOptions().position(Mar).title("Museu da arte"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Mar));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Mar, 15));
        LatLng Mb = new LatLng(-26.922602, -49.058963);
        mMap.addMarker(new MarkerOptions().position(Mb).title("Mausoleu Dr. Blumenau"));

        LatLng Tcg = new LatLng(-26.918521, -49.067902);
        mMap.addMarker(new MarkerOptions().position(Tcg).title("Teatro Carlos Gomes"));

        LatLng Mh = new LatLng(-26.925467, -49.0824708);
        mMap.addMarker(new MarkerOptions().position(Mh).title("Museu Hering"));

        LatLng Vg = new LatLng(-26.9155077, -49.087175);
        mMap.addMarker(new MarkerOptions().position(Vg).title("Vila Germânica"));

        LatLng Cspa = new LatLng(-26.919731, -49.066416);
        mMap.addMarker(new MarkerOptions().position(Cspa).title("Catedral São Paulo Apóstolo"));

        LatLng Prr = new LatLng(-26.9127837, -49.0881078);
        mMap.addMarker(new MarkerOptions().position(Prr).title("Parque Ramiro Ruediger"));



    }}
